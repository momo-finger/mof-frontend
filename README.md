# MoF frontend



## Description

[FR] Ici, nous maintenons le front de notre application, basé sur Vue. Nous faisons un big up! à notre équipe dévouée.\
[EN] Here we maintain our application UI, based on Vue. We give a big up! to our dedicated team.

<img src="screenshots/splash-screen.jpeg"  width="250" height="auto" alt="MoMo Finger splash screen">

<img src="screenshots/login.jpeg"  width="250" height="auto" alt="MoMo Finger splash screen">

<img src="screenshots/choose-country.jpeg"  width="250" height="auto" alt="MoMo Finger splash screen">

<img src="screenshots/Dashboard.jpeg"  width="250" height="auto" alt="MoMo Finger splash screen">

<img src="screenshots/signup-seller.jpeg"  width="250" height="auto" alt="MoMo Finger splash screen">

<img src="screenshots/check-validation-key.jpeg"  width="250" height="auto" alt="MoMo Finger splash screen">

<img src="screenshots/fingerprint.jpeg"  width="250" height="auto" alt="MoMo Finger splash screen">

<img src="screenshots/confirmation.jpeg"  width="250" height="auto" alt="MoMo Finger splash screen">

<img src="screenshots/history.jpeg"  width="250" height="auto" alt="MoMo Finger splash screen">


## Project Links
[FR] En dessous les différents liens d’accès au projet \
[EN] Below are the different links to access the project \

## MoMo Finger UI (Figma)
[Cliquez-ici / Here](https://bit.ly/3RU7lQy)

## MoMo Finger APP
[Cliquez-ici / Here](https://gitlab.com/momo-finger/mof-app.git)

## MoMo Finger Backend
[Cliquez-ici / Here](https://gitlab.com/momo-finger/mof-backend.git)

## MoMo Finger Front
[Cliquez-ici / Here](https://gitlab.com/momo-finger/mof-frontend.git)

## It‘s a IT company

[FR] Nous développons des innovations avec le duo (hard et soft) dans plusieurs domaines d’activité. En Agriculture, en Éducation, gestion de projet, gestion de finance.\
[EN] We develop innovations with the duo (hard and soft) in several fields of activity. In Agriculture, Education, Project Management, Finance Management.

[By Celerite Holding](https://celeriteholding.com)
Email: contact@celeriteholding.com

```
cd existing_repo
git remote add origin https://gitlab.com/momo-finger/mof-frontend.git
git branch -M main
git push -uf origin main
```


***

